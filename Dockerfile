FROM alpine as qemu

RUN if [ -n "${QEMU_ARCH}" ]; then \
		wget -O /qemu-${QEMU_ARCH}-static https://github.com/multiarch/qemu-user-static/releases/download/v7.2.0-1/qemu-${QEMU_ARCH}-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-${QEMU_ARCH}-static; \
	fi; \
	chmod a+x /qemu-${QEMU_ARCH}-static

FROM registry.gitlab.com/highercomve/tailscale:master

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/

ADD files /

RUN apk update; apk add --no-cache \
		jq \
		openrc \
		iptables \
		ip6tables \
		busybox-extras \
		bash \
		rsyslog

RUN rc-update add zeronetworking sysinit default; \
	rc-update add rsyslog sysinit default; \
	rc-update add ntpd default; \
	rc-update add tailscaled default; \
	rc-update add tailscale-client default; \
	echo root:pantacor | chpasswd

VOLUME [ "/var/lib/tailscale" ]

CMD [ "/sbin/init" ]
